import Joi from 'joi';

export const schemaForInsert = Joi.object({
    first_name: Joi.string()
        .alphanum()
        .min(1)
        .max(30)
        .required(),

    last_name: Joi.string()
        .alphanum()
        .min(1)
        .max(30)
        .required(),

    phone: Joi.string()
    .length(10)
    .pattern(/^[0-9]+$/)
    .required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        .required()
});

export const schemaForPatch = Joi.object({
    first_name: Joi.string()
        .alphanum()
        .min(1)
        .max(30),
        
    last_name: Joi.string()
        .alphanum()
        .min(1)
        .max(30),
       
    phone: Joi.string()
    .length(10)
    .pattern(/^[0-9]+$/),
   
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        
});
    
 

